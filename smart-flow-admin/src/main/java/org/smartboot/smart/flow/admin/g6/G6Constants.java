package org.smartboot.smart.flow.admin.g6;

/**
 * @author qinluo
 * @date 2023-02-23
 * @since 1.0.0
 */
public interface G6Constants {

    String COLOR = "color";

    /**
     * Color definition.
     */
    String ERROR = "#F4664A";
    String WARN = "#E3C800";
    String NORMAL = "#00CC00";
    String GERY = "#CCCCCC";
}
