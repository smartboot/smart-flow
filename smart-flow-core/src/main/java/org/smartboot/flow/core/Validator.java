package org.smartboot.flow.core;

/**
 * @author qinluo
 * @date 2022-11-13 22:54:54
 * @since 1.0.0
 */
public interface Validator {

    /**
     * Validate elements.
     */
    void validate();
}
