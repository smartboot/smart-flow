package org.smartboot.flow.core.common;

/**
 * @author qinluo
 * @date 2022/11/18 22:52
 * @since 1.0.0
 */
public class Uniqueness {

    /**
     * Unique identifier.
     */
    protected String identifier;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
