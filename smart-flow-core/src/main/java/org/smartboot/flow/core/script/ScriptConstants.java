package org.smartboot.flow.core.script;

/**
 * Constants definitions in script.
 *
 * @author qinluo
 * @date 2022/11/28 19:41
 * @since 1.0.0
 */
public interface ScriptConstants {

    /**
     * Request params.
     */
    String REQ = "request";

    /**
     * Result mode.
     */
    String RESULT = "result";

    /**
     * Flow engine execution context.
     */
    String CONTEXT = "context";

    /**
     * Alias for context.
     *
     * @since 1.0.5
     */
    String CTX = "ctx";
}
