package org.smartboot.flow.manager.metric;

/**
 * @author qinluo
 * @date 2022/11/23 21:45
 * @since 1.0.0
 */
public enum MetricKind {

    /**
     * 累加
     */
    ACCUMULATE,

    /**
     * 最大
     */
    MAX,
}
